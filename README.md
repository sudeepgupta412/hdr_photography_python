# HDR_photography_python

In this project, we work on algorithms behind computing HDR images based on the paper “Recovering High Dynamic Range Radiance Maps from Photographs” by Debevec & Malik.

##Instructions to run the code:
	1) copy images in sample directory
	2) run main.py
	3) output will be generated in output
	
	
##Sample Output:
![output](output/output.png)